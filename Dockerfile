# Copyright (c) 2021, Oracle and/or its affiliates.
# Licensed under the Universal Permissive License v 1.0 as shown at https://oss.oracle.com/licenses/upl.

# This is a sample Dockerfile for supplying Model in Image model files
# and a WDT install in a small separate auxiliary image
# image. This is an alternative to supplying the files directly
# in the domain resource `domain.spec.image` image.

# AUXILIARY_IMAGE_PATH arg:
#   Parent location for Model in Image model and WDT installation files.
#   Must match domain resource 'domain.spec.auxiliaryImageVolumes.mountPath'
#   For model-in-image, the following two domain resource attributes can
#   be a directory in the mount path:
#     1) 'domain.spec.configuration.model.modelHome'
#     2) 'domain.spec.configuration.model.wdtInstallHome'
#   Default '/auxiliary'.
#

FROM maven:3.9.4-eclipse-temurin-17-alpine as java-build
ARG GROUP_ID
ARG ARTIFACT_ID
ARG VERSION=1-SNAPSHOT
ARG PACKAGING=war
WORKDIR /app
COPY settings.xml /app
RUN mvn org.apache.maven.plugins:maven-dependency-plugin:3.6.0:get org.apache.maven.plugins:maven-dependency-plugin:3.6.0:copy -s settings.xml -Dartifact=$GROUP_ID:$ARTIFACT_ID:$VERSION:$PACKAGING -DoutputDirectory=./

FROM alpine:3.18.3 as archive-build
ARG ARTIFACT_ID
ARG VERSION=1-SNAPSHOT
ARG PACKAGING=war
RUN apk add --no-cache zip
WORKDIR /archive
COPY --from=java-build ./app/$ARTIFACT_ID-${VERSION}.$PACKAGING ./wlsdeploy/applications/$ARTIFACT_ID/$ARTIFACT_ID-${VERSION}.$PACKAGING
RUN zip -r archive.zip wlsdeploy

FROM alpine:3.18.3 as model-build
ARG ARTIFACT_ID
ARG VERSION=1-SNAPSHOT
ARG PACKAGING=war
ARG TYPE=Application
ARG MANAGED_SERVER
RUN apk add --no-cache gettext
WORKDIR /models
COPY ./models /templates
RUN envsubst < /templates/template.model.yaml >> $ARTIFACT_ID.model.yaml

FROM busybox
ARG ARTIFACT_ID
ARG AUXILIARY_IMAGE_PATH=/auxiliary
ARG USER=oracle
ARG USERID=1000
ARG GROUP=root
ENV AUXILIARY_IMAGE_PATH=${AUXILIARY_IMAGE_PATH}
RUN adduser -D -u ${USERID} -G $GROUP $USER
COPY --from=model-build ./models ${AUXILIARY_IMAGE_PATH}/models
COPY --from=archive-build /archive/archive.zip ${AUXILIARY_IMAGE_PATH}/models/$ARTIFACT_ID.zip
RUN chown -R $USER:$GROUP ${AUXILIARY_IMAGE_PATH}/
USER $USER
